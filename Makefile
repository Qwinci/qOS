CC=clang
LD=lld-link
LDFLAGS+=/entry:efi_main /subsystem:efi_application /nodefaultlib /debug
CFLAGS=--target=x86_64-unknown-windows -ffreestanding -mno-red-zone -nostdlib
CFLAGS+=-Wall -Iinclude/uefi/ -Iinclude/uefi/X64 -I/usr/lib/clang/12.0.1/include/ -I/usr/include/c++/v1

ASM=nasm
ASMFLAGS=

OVMFDIR=/usr/share/OVMF/x64

KERNEL_LD=ld.lld
# --target=x86_64-pc-none-elf
KERNEL_CFLAGS+=-fshort-wchar -static -ffreestanding -fno-stack-protector -Wno-interrupt-service-routine
KERNEL_CFLAGS+=-m64 -mno-red-zone -Isrc/kernel/std -Ikernel/ -I/usr/lib/clang/12.0.1/include/ -I/usr/include/c++/v1
KERNEL_CFLAGS+=-fno-exceptions
KERNEL_LDFLAGS=--entry=start --nostdlib -Tsrc/kernel/kernel.ld -static

SRC=$(wildcard src/bootloader/*.cpp)
OBJ=$(patsubst src/bootloader/%.cpp, build/bootloader/%.o, $(SRC))
OUTDIR=build
SRCDIR=src

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

KERNELSRC=$(call rwildcard, $(SRCDIR)/kernel,*.cpp)
KERNELOBJ=$(patsubst $(SRCDIR)/%.cpp, $(OUTDIR)/%.o, $(KERNELSRC))

KERNELASM=$(call rwildcard, $(SRCDIR)/kernel,*.asm)
KERNELASMOBJ=$(patsubst $(SRCDIR)/kernel/%.asm, $(OUTDIR)/kernel/asm/%.o, $(KERNELASM))

.PHONY: all
.PHONY: update_fat

all: qos.efi kernel.elf update_fat

qos.efi: $(OBJ)
	@echo LINKING $@
	@$(LD) $(LDFLAGS) $^ /out:$(OUTDIR)/$@

kernel.elf: $(KERNELOBJ) $(KERNELASMOBJ) $(OUTDIR)/kernel/interrupts/interrupts.o
	@echo LINKING KERNEL
	@$(KERNEL_LD) $(KERNEL_LDFLAGS) $^ -o $(OUTDIR)/$@

$(OUTDIR)/bootloader/%.o: $(SRCDIR)/bootloader/%.cpp
	@mkdir -p $(@D)
	@echo COMPILING $^
	@$(CC) $(CFLAGS) -c $^ -o $@

$(OUTDIR)/kernel/%.o: $(SRCDIR)/kernel/%.cpp
	@mkdir -p $(@D)
	@echo COMPILING $^
	@$(CC) $(KERNEL_CFLAGS) -c $^ -o $@

$(OUTDIR)/kernel/asm/%.o: $(SRCDIR)/kernel/%.asm
	@mkdir -p $(@D)
	@echo COMPILING ASM $^
	@$(ASM) $(ASMFLAGS) $^ -f elf64 -o $@

update_fat: qos.efi kernel.elf
	@echo UPDATING IMAGE FILE
	@cp build/qos.efi build/BOOTX64.EFI
	@mcopy -D o -i build/fat.img build/BOOTX64.EFI ::/EFI/BOOT
	@mcopy -D o -i build/fat.img build/kernel.elf ::
	@mcopy -D o -i build/fat.img resources/zap-vga16.psf ::

run: update_fat
# --no-reboot --no-shutdown -d int
	qemu-system-x86_64 -cpu qemu64 -drive file=build/fat.img,format=raw -m 2G -drive if=pflash,format=raw,unit=0,file="$(OVMFDIR)/OVMF_CODE.fd",readonly=on -drive if=pflash,format=raw,unit=1,file="$(OVMFDIR)/OVMF_VARS.fd" -net none

clean: remove create_fat

remove:
	@ rm -rf build/*
create_fat:
	@dd if=/dev/zero of=build/fat.img bs=1k count=1440 status=none
	@mformat -i build/fat.img -f 1440 ::
	@mmd -i build/fat.img ::/EFI
	@mmd -i build/fat.img ::/EFI/BOOT
