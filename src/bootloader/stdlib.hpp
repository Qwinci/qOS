#include <cstdint>
#include "Uefi.h"

inline void IoWrite8(uint16_t port, uint8_t value);
inline uint8_t IoRead8(uint16_t port);

void printf(EFI_SYSTEM_TABLE* st, const wchar_t* format, ...);