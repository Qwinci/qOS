#include "Uefi.h"
#include "../../include/GNU/elf.h"
#include "Protocol/GraphicsOutput.h"
#include "Guid/FileInfo.h"
#include "stdlib.hpp"
#include "utils.hpp"
#include "Guid/Acpi.h"

EFI_SYSTEM_TABLE *st;
EFI_BOOT_SERVICES *bs;
EFI_RUNTIME_SERVICES *rs;
EFI_HANDLE handle;

struct FrameBuffer
{
	void *BaseAddress;
	unsigned long long BufferSize;
	unsigned int Width;
	unsigned int Height;
	unsigned int PixelsPerScanline;
};

#define PSF1_MAGIC0 0x36
#define PSF1_MAGIC1 0x04

#pragma pack(1)
struct PSF1_HEADER {
	uint8_t magic[2];
	uint8_t mode;
	uint8_t charSize;
};
#pragma pack(0)
struct PSF1_FONT {
	PSF1_HEADER* psf1Header;
	void* glyphBuffer;
};

PSF1_FONT* LoadPSF1Font(EFI_SYSTEM_TABLE* systemTable, EFI_BOOT_SERVICES* bootServices, EFI_HANDLE imageHandle, EFI_FILE* directory, const wchar_t* path) {
	EFI_FILE* font = LoadFile(imageHandle, bootServices, directory, path);
	if (font == nullptr) return nullptr;

	auto* fontHeader = static_cast<PSF1_HEADER *>(malloc(bootServices, sizeof(PSF1_HEADER)));
	UINTN size = sizeof(PSF1_HEADER);
	font->Read(font, &size, fontHeader);

	if (fontHeader->magic[0] != PSF1_MAGIC0 || fontHeader->magic[1] != PSF1_MAGIC1) {
		return nullptr;
	}

	UINTN glyphBufferSize = fontHeader->charSize * 256;
	if (fontHeader->mode == 1) {
		glyphBufferSize = fontHeader->charSize * 512;
	}

	void* glyphBuffer = malloc(bootServices, glyphBufferSize);
	{
		font->SetPosition(font, sizeof(PSF1_HEADER));
		font->Read(font, &glyphBufferSize, glyphBuffer);
	}

	auto* finishedFont = static_cast<PSF1_FONT *>(malloc(bootServices, sizeof(PSF1_FONT)));
	finishedFont->psf1Header = fontHeader;
	finishedFont->glyphBuffer = glyphBuffer;
	return finishedFont;
}

FrameBuffer *InitializeGOP()
{
	EFI_GUID gopGuid = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
	EFI_GRAPHICS_OUTPUT_PROTOCOL *gop;
	EFI_STATUS status;

	status = bs->LocateProtocol(&gopGuid, nullptr, (void **) &gop);
	if (EFI_ERROR(status))
	{
		printf(st, L"Unable to locate GOP\r\n");
		return nullptr;
	} else
	{
		printf(st, L"GOP located\r\n");
	}

	auto* frameBuffer = static_cast<FrameBuffer*>(malloc(bs, sizeof(FrameBuffer)));
	frameBuffer->BaseAddress = (void *) gop->Mode->FrameBufferBase;
	frameBuffer->BufferSize = gop->Mode->FrameBufferSize;
	frameBuffer->Width = gop->Mode->Info->HorizontalResolution;
	frameBuffer->Height = gop->Mode->Info->VerticalResolution;
	frameBuffer->PixelsPerScanline = gop->Mode->Info->PixelsPerScanLine;
	return frameBuffer;
}

struct BootInfo {
	FrameBuffer* frameBuffer;
	PSF1_FONT* font;
	EFI_MEMORY_DESCRIPTOR* mMap;
	UINTN mapSize;
	UINTN mapDescSize;
	void* rsdp;
};

INTN CompareGuid(EFI_GUID* Guid1, EFI_GUID* Guid2) {
	INT32* g1, *g2, r;
	g1 = reinterpret_cast<INT32*>(Guid1);
	g2 = reinterpret_cast<INT32*>(Guid2);
	r  = g1[0] - g2[0];
	r |= g1[1] - g2[1];
	r |= g1[2] - g2[2];
	r |= g1[3] - g2[3];
	return r;
}

UINTN strcmp(const CHAR8* const a, const CHAR8* const b, UINTN length) {
	for (UINTN i = 0; i < length; ++i) {
		if (a[i] != b[i]) return 0;
	}
	return 1;
}

EFI_STATUS efi_main(EFI_HANDLE efiHandle, EFI_SYSTEM_TABLE *systemTable)
{
	st = systemTable;
	rs = systemTable->RuntimeServices;
	bs = systemTable->BootServices;
	handle = efiHandle;

	st->ConOut->ClearScreen(st->ConOut);

	EFI_FILE *Kernel = LoadFile(efiHandle, bs, nullptr, L"kernel.elf");
	if (Kernel == nullptr)
	{
		printf(st, L"Could not load the kernel.");
	} else
	{
		printf(st, L"Kernel file successfully located!\r\n");
	}

	Elf64_Ehdr header;
	{
		UINTN FileInfoSize;
		EFI_FILE_INFO *FileInfo;
		Kernel->GetInfo(Kernel, &gEfiFileInfoGuid, &FileInfoSize, nullptr);
		bs->AllocatePool(EfiLoaderData, FileInfoSize, (void **) &FileInfo);
		Kernel->GetInfo(Kernel, &gEfiFileInfoGuid, &FileInfoSize, (void *) FileInfo);

		UINTN size = sizeof(Elf64_Ehdr);

		Kernel->Read(Kernel, &size, &header);
	}
	if (
			memcmp(&header.e_ident[EI_MAG0], ELFMAG, SELFMAG) != 0 ||
			header.e_ident[EI_CLASS] != ELFCLASS64 ||
			header.e_ident[EI_DATA] != ELFDATA2LSB ||
			header.e_type != ET_EXEC ||
			header.e_machine != EM_X86_64 ||
			header.e_version != EV_CURRENT
			)
	{
		printf(st, L"Kernel format is bad\r\n");
	} else {
		printf(st, L"Kernel header successfully verified!\r\n");
	}

	// Program headers
	Elf64_Phdr* phdrs;
	{
		Kernel->SetPosition(Kernel, header.e_phoff);
		UINTN size = header.e_phnum * header.e_phentsize;
		bs->AllocatePool(EfiLoaderData, size, (void **) &phdrs);
		Kernel->Read(Kernel, &size, phdrs);
	}

	for (
			Elf64_Phdr *phdr = phdrs;
			(char *) phdr < (char*) phdrs + header.e_phnum * header.e_phentsize;
			phdr = (Elf64_Phdr*) ((char*) phdr + header.e_phentsize)
			)
	{
		switch (phdr->p_type)
		{
			case PT_LOAD:
				unsigned int pages = (phdr->p_memsz + 0x1000 - 1) / 0x1000;
				Elf64_Addr segment = phdr->p_paddr;
				bs->AllocatePages(AllocateAddress, EfiLoaderData, pages, reinterpret_cast<EFI_PHYSICAL_ADDRESS*>(&segment));

				Kernel->SetPosition(Kernel, phdr->p_offset);
				UINTN size = phdr->p_filesz;
				Kernel->Read(Kernel, &size, (void *) segment);
				break;
		}
	}

	printf(st, L"Kernel Loaded Successfully!\r\n");

	FrameBuffer *frameBuffer = InitializeGOP();

	printf(st, L"Base: %d\r\n", (long long) frameBuffer->BaseAddress);
	printf(st, L"Size: %d\r\n", (long long) frameBuffer->BufferSize);
	printf(st, L"Width: %d\r\n", frameBuffer->Width);
	printf(st, L"height: %d\r\n", frameBuffer->Height);
	printf(st, L"PixelsPerScanLine: %d\r\n", frameBuffer->PixelsPerScanline);

	auto* font = LoadPSF1Font(systemTable, bs, efiHandle, nullptr, L"zap-vga16.psf");
	if (font == nullptr) {
		printf(st, L"Font is not valid or is not found.\r\n");
	}
	else {
		printf(st, L"Found found. char size: %d\r\n", font->psf1Header->charSize);
	}

	EFI_MEMORY_DESCRIPTOR* map = nullptr;
	UINTN mapSize, mapKey;
	UINTN descriptorSize;
	UINT32 descriptorVersion;
	{
		EFI_STATUS status = bs->GetMemoryMap(&mapSize, map, &mapKey, &descriptorSize, &descriptorVersion);
		mapSize += 2 * descriptorSize;
		map = static_cast<EFI_MEMORY_DESCRIPTOR*>(malloc(bs, mapSize));
		status = bs->GetMemoryMap(&mapSize, map, &mapKey, &descriptorSize, &descriptorVersion);
		if (status != EFI_SUCCESS) {
			printf(st, L"Failed to get memory map.\r\n");
			bs->Stall(4000);
			bs->Exit(efiHandle, EFI_DEVICE_ERROR, 0, nullptr);
		}
	}

	EFI_CONFIGURATION_TABLE* configTable = st->ConfigurationTable;
	void* rsdp = nullptr;
	EFI_GUID Acpi2TableGuid = EFI_ACPI_20_TABLE_GUID;

	for (UINTN i = 0; i < st->NumberOfTableEntries; ++i) {
		if (CompareGuid(&configTable[i].VendorGuid, &Acpi2TableGuid)) {
			if (strcmp("RSD PTR ", static_cast<const CHAR8*>(configTable->VendorTable), 8) != 0) {
				rsdp = configTable->VendorTable;
			}
		}
	}

	void (*KernelStart)(BootInfo*)__attribute__((sysv_abi)) = ((__attribute__((sysv_abi)) void (*)(
			BootInfo*)) header.e_entry);

	auto* bootInfo = static_cast<BootInfo*>(malloc(bs, sizeof(BootInfo)));
	bootInfo->frameBuffer = frameBuffer;
	bootInfo->font = font;
	bootInfo->mMap = map;
	bootInfo->mapSize = mapSize;
	bootInfo->mapDescSize = descriptorSize;
	bootInfo->rsdp = rsdp;

	st->ConOut->ClearScreen(st->ConOut);

	EFI_STATUS status;
	do {
		status = bs->ExitBootServices(efiHandle, mapKey);
		if (status != EFI_SUCCESS) {
			bs->GetMemoryMap(&mapSize, map, &mapKey, &descriptorSize, &descriptorVersion);
		}
	} while(status != EFI_SUCCESS);

	KernelStart(bootInfo);
	return EFI_SUCCESS;
}