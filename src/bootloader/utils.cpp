#include "utils.hpp"
#include "Protocol/LoadedImage.h"
#include "Guid/FileInfo.h"

void* malloc(EFI_BOOT_SERVICES* bs, size_t size)
{
	EFI_STATUS status;
	void *buffer;

	status = bs->AllocatePool(EfiLoaderData, size, &buffer);
	if (EFI_ERROR(status)) return nullptr;
	return buffer;
}

void free(EFI_BOOT_SERVICES* bs, void* ptr)
{
	bs->FreePool(ptr);
}

int memcmp(const void* aptr, const void* bptr, size_t n)
{
	const auto* a = static_cast<const unsigned char *>(aptr);
	const auto* b = static_cast<const unsigned char *>(bptr);
	for (size_t i = 0; i < n; ++i)
	{
		if (a[i] < b[i]) return -1;
		else if (a[i] > b[i]) return 1;
	}
	return 0;
}

static EFI_GUID gEfiSimpleFileSystemProtocolGuid = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
static EFI_GUID gEfiLoadedImageProtocolGuid = EFI_LOADED_IMAGE_PROTOCOL_GUID;
static EFI_GUID gEfiFileInfoGuid = EFI_FILE_INFO_ID;

EFI_FILE* LoadFile(EFI_HANDLE handle, EFI_BOOT_SERVICES* bs, EFI_FILE* Directory, const wchar_t* Path)
{
	EFI_FILE *LoadedFile;

	EFI_LOADED_IMAGE_PROTOCOL *LoadedImage;
	bs->HandleProtocol(handle, &gEfiLoadedImageProtocolGuid, (void **) &LoadedImage);

	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL *FileSystem;
	bs->HandleProtocol(LoadedImage->DeviceHandle, &gEfiSimpleFileSystemProtocolGuid, (void **) &FileSystem);

	if (Directory == nullptr)
	{
		FileSystem->OpenVolume(FileSystem, &Directory);
	}

	EFI_STATUS status = Directory->Open(Directory, &LoadedFile, (CHAR16 *) Path, EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY);
	if (status != EFI_SUCCESS)
	{
		return nullptr;
	}
	return LoadedFile;
}