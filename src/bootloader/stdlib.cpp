#include <cstdarg>
#include "stdlib.hpp"

inline void IoWrite8(uint16_t port, uint8_t value)
{
	__asm __volatile__ ("outb %0, %1"
	:
	: "a"(value), "Nd"(port));
}

inline uint8_t IoRead8(uint16_t port)
{
	uint8_t ret;
	__asm__ __volatile__ ("inb %1, %0"
	: "=a"(ret)
	: "Nd"(port));
	return ret;
}

void print(EFI_SYSTEM_TABLE* st, CHAR16* string)
{
	EFI_STATUS status = st->ConOut->OutputString(st->ConOut, string);
	if (status != EFI_SUCCESS)
	{
		st->ConOut->OutputString(st->ConOut, reinterpret_cast<CHAR16*>(const_cast<wchar_t*>(L"Print failed.\r\n")));
	}
}

void printchar(EFI_SYSTEM_TABLE* st, CHAR16 character)
{
	CHAR16 string[2];
	string[1] = 0;
	string[0] = character;
	EFI_STATUS status = st->ConOut->OutputString(st->ConOut, string);
	if (status != EFI_SUCCESS)
	{
		st->ConOut->OutputString(st->ConOut, reinterpret_cast<CHAR16*>(const_cast<wchar_t*>(L"Print failed.\r\n")));
	}
}

void printnumber(EFI_SYSTEM_TABLE* st, long long value)
{
	int index = 20;
	CHAR16 characters[21];

	characters[index] = 0;
	while (value)
	{
		characters[--index] = '0' + value % 10;
		value /= 10;
	}
	st->ConOut->OutputString(st->ConOut, characters + index);
}

unsigned char hex_table[] = "0123456789ABCDEF";

void printhex(EFI_SYSTEM_TABLE* st, long long value)
{
	unsigned int index = 20;
	CHAR16 characters[21];
	characters[index] = 0;

	if (value == 0)
	{
		characters[--index] = '0';
	}
	while (value)
	{
		long long modulo = value % 16;
		characters[--index] = hex_table[modulo];
		value /= 16;
	}
	if (index > 18)
	{
		characters[--index] = '0';
	}
	st->ConOut->OutputString(st->ConOut, characters + index);
}

void printf(EFI_SYSTEM_TABLE* st, const wchar_t* format, ...)
{
	va_list valist;
	unsigned int argc = 0;
	for (int i = 0; format[i] != 0; ++i)
	{
		if (format[i] == '%')
		{
			argc++;
		}
	}
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wvarargs"
	va_start(valist, argc);
#pragma clang diagnostic pop
	for (int i = 0; format[i] != 0; ++i)
	{
		if (format[i] == '%')
		{
			switch (format[++i])
			{
				case 'c':
					printchar(st, va_arg(valist, unsigned int));
					break;
				case 'd':
				case 'i':
					printnumber(st, va_arg(valist, long long));
					break;
				case 's':
					print(st, va_arg(valist, CHAR16*));
					break;
				case 'u':
					printnumber(st, va_arg(valist, unsigned long long));
				case 'x':
					printhex(st, va_arg(valist, unsigned long long));
					break;
				case 'p':
					printhex(st, (long long) va_arg(valist, void*));
					break;
				default:
					break;
			}
		} else
		{
			printchar(st, format[i]);
		}
	}

	va_end(valist);
}
