#pragma once
#include "Uefi.h"
#include "Protocol/SimpleFileSystem.h"

void* malloc(EFI_BOOT_SERVICES* bs, size_t size);
void free(EFI_BOOT_SERVICES* bs, void* ptr);

int memcmp(const void* aptr, const void* bptr, size_t n);;

EFI_FILE* LoadFile(EFI_HANDLE handle, EFI_BOOT_SERVICES* bs, EFI_FILE* Directory, const wchar_t* Path);