#include "include/string.hpp"

char string[128];

const char* to_hstring(uint64_t integer) {
	/*uint8_t size = 8 * 2 - 1;
	for (uint8_t i = size; i >= 0; --i) {
		char remainder = static_cast<char>(integer & 0b1111);
		string[i] = remainder > 9 ? static_cast<char>(remainder - 10 + 'A') : static_cast<char>(remainder + '0');
		if (integer != 0) {
			integer >>= 4;
		}
		if (i == 0) break;
	}*/

	uint8_t size = 0;
	uint64_t tmp = integer;
	while (tmp > 0) {
		tmp >>= 4;
		++size;
	}

	for (uint8_t i = size - 1; i >= 0; --i) {
		char remainder = static_cast<char>(integer & 0b1111);
		string[i] = remainder > 9 ? static_cast<char>(remainder - 10 + 'A') : static_cast<char>(remainder + '0');
		if (integer != 0) {
			integer >>= 4;
		}

		if (i == 0) break;
	}
	string[size + 1] = 0;
	return string;
}

const char* to_string(uint64_t integer) {
	uint8_t size = 0;
	uint64_t sizeTest = integer;
	while (sizeTest / 10 > 0) {
		sizeTest /= 10;
		++size;
	}

	uint8_t index = 0;
	while (integer / 10 > 0) {
		uint8_t remainder = integer % 10;
		integer /= 10;
		string[size - index] = remainder + '0';
		++index;
	}
	uint8_t remainder = integer % 10;
	string[size - index] = remainder + '0';
	string[size + 1] = 0;
	return string;
}
const char* to_string(int64_t integer) {
	uint8_t isNegative = 0;

	if (integer < 0) {
		isNegative = 1;
		integer *= -1;
		string[0] = '-';
	}

	uint8_t size = 0;
	uint64_t sizeTest = integer;
	while (sizeTest / 10 > 0) {
		sizeTest /= 10;
		++size;
	}

	uint8_t index = 0;
	while (integer / 10 > 0) {
		uint8_t remainder = integer % 10;
		integer /= 10;
		string[isNegative + size - index] = remainder + '0';
		++index;
	}
	uint8_t remainder = integer % 10;
	string[isNegative + size - index] = remainder + '0';
	string[isNegative + size + 1] = 0;
	return string;
}

const char* to_string(double value, uint8_t decimalPlaces) {
	if (decimalPlaces > 20) decimalPlaces = 20;

	to_string(static_cast<int64_t>(value));

	if (value < 0) {
		value *= -1;
	}

	uint64_t index = 0;
	while (string[index] != 0) {
		++index;
	}
	string[index++] = '.';

	double newValue = value - static_cast<int>(value);

	for (uint8_t i = 0; i < decimalPlaces; ++i) {
		newValue *= 10;
		string[index++] = static_cast<int>(newValue) + '0';
		newValue -= static_cast<int>(newValue);
	}

	string[index + 1] = 0;

	return string;
}