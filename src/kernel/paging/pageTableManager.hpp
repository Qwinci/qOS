#pragma once
#include "paging.hpp"

class PageTableManager {
public:
	explicit PageTableManager(PageTable* PML4Address);
	PageTableManager() = default;
	PageTable* PML4;
	void MapMemory(void* virtualMemory, void* physicalMemory) const;
};

extern PageTableManager g_PageTableManager;