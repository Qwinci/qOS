#pragma once
#include <cstdint>
#include "../include/bitmap.hpp"
#include "../include/memory.hpp"

class PageFrameAllocator {
public:
	void ReadEFIMemoryMap(EFI_MEMORY_DESCRIPTOR* mMap, size_t mMapSize, size_t mMapDescSize);
	Bitmap PageBitmap {};
	void FreePage(void* address) const;
	void LockPage(void* address) const;
	void FreePages(void* address, uint64_t pageCount) const;
	void LockPages(void* address, uint64_t pageCount) const;
	void* RequestPage() const;

	uint64_t GetFreeRAM();
	uint64_t GetUsedRAM();
	uint64_t GetReservedRAM();

private:
	void initBitmap(size_t bitmapSize, void* bufferAddress);
	void ReservePage(void* address) const;
	void UnReservePage(void* address) const;
	void ReservePages(void* address, uint64_t pageCount);
	void UnReservePages(void* address, uint64_t pageCount);
};

extern PageFrameAllocator GlobalAllocator;