#include "pageframeallocator.hpp"

uint64_t freeMemory = 0;
uint64_t reservedMemory = 0;
uint64_t usedMemory = 0;
static bool initialized = false;
PageFrameAllocator GlobalAllocator;

void PageFrameAllocator::ReadEFIMemoryMap(EFI_MEMORY_DESCRIPTOR *mMap, size_t mMapSize, size_t mMapDescSize) {
	if (initialized) return;

	initialized = true;

	uint64_t mMapEntries = mMapSize / mMapDescSize;

	void* largestFreeMemSeg = nullptr;
	size_t largestFreeMemSegSize = 0;

	for (int i = 0; i < mMapEntries; ++i) {
		auto* desc = (EFI_MEMORY_DESCRIPTOR*)((uint64_t)mMap + (i * mMapDescSize));
		if (desc->Type == 7) { // EfiConventionalMemory
			if (desc->NumberOfPages * 4096 > largestFreeMemSegSize) {
				largestFreeMemSeg = desc->PhysicalStart;
				largestFreeMemSegSize = desc->NumberOfPages * 4096;
			}
		}
	}

	uint64_t memorySize = GetMemorySize(mMap, mMapSize, mMapDescSize);
	freeMemory = memorySize;
	uint64_t bitmapSize = memorySize / 4096 / 8 + 1;

	// Initialize bitmap
	initBitmap(bitmapSize, largestFreeMemSeg);

	// Reserve all memory
	ReservePages(nullptr, memorySize / 4096 + 1);

	// reserve pages of unusable/reserved memory
	for (int i = 0; i < mMapEntries; ++i) {
		auto* desc = (EFI_MEMORY_DESCRIPTOR*)((uint64_t)mMap + (i * mMapDescSize));
		if (desc->Type == 7) { // not EfiConventionalMemory
			UnReservePages(desc->PhysicalStart, desc->NumberOfPages);
		}
	}

	// Reserve between 0 and 0x100000
	ReservePages(nullptr, 0x100);

	// lock pages of bitmap
	LockPages(PageBitmap.Buffer, PageBitmap.Size / 4096 + 1);
}

void PageFrameAllocator::initBitmap(size_t bitmapSize, void* bufferAddress) {
	PageBitmap.Size = bitmapSize;
	PageBitmap.Buffer = static_cast<uint8_t*>(bufferAddress);
	for (size_t i = 0; i < bitmapSize; ++i) {
		*(reinterpret_cast<uint8_t*>(PageBitmap.Buffer) + i) = 0;
	}
}

void PageFrameAllocator::FreePages(void *address, uint64_t pageCount) const {
	for (size_t i = 0; i < pageCount; ++i) {
		FreePage((void*)((uint64_t)address + (i * 4096)));
	}
}

void PageFrameAllocator::LockPages(void *address, uint64_t pageCount) const {
	for (size_t i = 0; i < pageCount; ++i) {
		LockPage((void*)((uint64_t)address + (i * 4096)));
	}
}

void PageFrameAllocator::UnReservePages(void *address, uint64_t pageCount) {
	for (size_t i = 0; i < pageCount; ++i) {
		UnReservePage((void*)((uint64_t)address + (i * 4096)));
	}
}

void PageFrameAllocator::ReservePages(void *address, uint64_t pageCount) {
	for (size_t i = 0; i < pageCount; ++i) {
		ReservePage((void*)((uint64_t)address + (i * 4096)));
	}
}

void PageFrameAllocator::LockPage(void *address) const {
	uint64_t index = (uint64_t) address / 4096;
	if (PageBitmap[index]) return;
	if (PageBitmap.Set(index, true)) {
		freeMemory -= 4096;
		usedMemory += 4096;
	}
}

void PageFrameAllocator::ReservePage(void *address) const {
	uint64_t index = (uint64_t) address / 4096;
	if (PageBitmap[index]) return;
	if (PageBitmap.Set(index, true)) {
		freeMemory -= 4096;
		reservedMemory += 4096;
	}
}

uint64_t PageFrameAllocator::GetFreeRAM() {
	return freeMemory;
}
uint64_t PageFrameAllocator::GetUsedRAM() {
	return usedMemory;
}
uint64_t PageFrameAllocator::GetReservedRAM() {
	return reservedMemory;
}

uint64_t pageBitmapIndex = 0;
void* PageFrameAllocator::RequestPage() const {
	for (; pageBitmapIndex < PageBitmap.Size >> 3; ++pageBitmapIndex) {
		if (PageBitmap[pageBitmapIndex]) continue;
		LockPage(reinterpret_cast<void*>(pageBitmapIndex * 4096));
		return reinterpret_cast<void*>(pageBitmapIndex * 4096);
	}

	return nullptr; // Page Frame Swap to file
}

void PageFrameAllocator::FreePage(void *address) const {
	uint64_t index = (uint64_t) address / 4096;
	if (!PageBitmap[index]) return;
	if (PageBitmap.Set(index, false)) {
		freeMemory += 4096;
		usedMemory -= 4096;
		if (pageBitmapIndex > index) pageBitmapIndex = index;
	}
}

void PageFrameAllocator::UnReservePage(void *address) const {
	uint64_t index = (uint64_t) address / 4096;
	if (!PageBitmap[index]) return;
	if (PageBitmap.Set(index, false)) {
		freeMemory += 4096;
		reservedMemory -= 4096;
		if (pageBitmapIndex > index) pageBitmapIndex = index;
	}
}
