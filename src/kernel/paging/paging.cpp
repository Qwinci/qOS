#include "paging.hpp"

void PageDirectoryEntry::SetFlag(PT_Flag flag, bool enabled) {
	uint64_t bitSelector = 1 << static_cast<uint64_t>(flag);
	value &= ~bitSelector;
	if (enabled) {
		value |= bitSelector;
	}
}

bool PageDirectoryEntry::GetFlag(PT_Flag flag) const {
	uint64_t bitSelector = 1 << static_cast<uint64_t>(flag);
	return ((value & bitSelector) > 0) != 0;
}

void PageDirectoryEntry::SetAddress(uint64_t address) {
	address &= 0x000000FFFFFFFFFF;
	value &= 0xFFF0000000000FFF;
	value |= (address << 12);
}

uint64_t PageDirectoryEntry::GetAddress() const {
	return (value & 0x000FFFFFFFFFF000) >> 12;
}
