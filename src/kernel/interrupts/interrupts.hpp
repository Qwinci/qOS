#pragma once
#include "../include/basicRenderer.hpp"

#define PIC1_COMMAND 0x20
#define PIC1_DATA 0x21
#define PIC2_COMMAND 0xA0
#define PIC2_DATA 0xA1
#define PIC_EOI 0x20

#define ICW1_INIT 0x10
#define ICW1_ICW4 0x01
#define ICW4_8086 0x01

struct interrupt_frame {
	uint64_t instruction_pointer;
	uint16_t code_segment;
	uint64_t rflags;
	uint64_t stack_pointer;
	uint16_t stack_segment;
} __attribute__((packed));

__attribute__((interrupt)) void PageFault_Handler(__attribute__((unused)) interrupt_frame* frame);
__attribute__((interrupt)) void DoubleFault_Handler(__attribute__((unused))interrupt_frame* frame);
__attribute__((interrupt)) void GPFault_Handler(__attribute__((unused)) interrupt_frame* frame);

__attribute__((interrupt)) void KeyboardInt_Handler(__attribute__((unused)) interrupt_frame* frame);

void RemapPIC();
__attribute__((no_caller_saved_registers))void PIC_EndMaster();

__attribute__((unused)) __attribute__((no_caller_saved_registers)) void PIC_EndSlave();