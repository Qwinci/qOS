#include "interrupts.hpp"
#include "../include/panic.hpp"
#include "../include/IO.hpp"
#include "../userinput/keyboard.hpp"
#include "../include/string.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Winterrupt-service-routine"
__attribute__((interrupt)) void PageFault_Handler(__attribute__((unused)) interrupt_frame* frame) {
	Panic("Page Fault Detected");
	int64_t Register = 0;

	GlobalRenderer->Next();
	GlobalRenderer->Print("Register dump: ");
	GlobalRenderer->Next();
	GlobalRenderer->Print("rax: ");
	asm volatile("mov %%rax, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");
	GlobalRenderer->Print("rcx: ");
	asm volatile("mov %%rcx, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");
	GlobalRenderer->Print("rdx: ");
	asm volatile("mov %%rdx, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");
	GlobalRenderer->Print("rbx: ");
	asm volatile("mov %%rbx, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");

	GlobalRenderer->Next();

	GlobalRenderer->Print("rsp: ");
	asm volatile("mov %%rsp, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");
	GlobalRenderer->Print("rbp: ");
	asm volatile("mov %%rbp, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");
	GlobalRenderer->Print("rsi: ");
	asm volatile("mov %%rsi, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Print("  ");
	GlobalRenderer->Print("rdi: ");
	asm volatile("mov %%rdi, %0" : "=r"(Register));
	GlobalRenderer->Print("0x");
	GlobalRenderer->Print(to_hstring(Register));
	GlobalRenderer->Next();

	asm("hlt");
}

__attribute__((interrupt)) void DoubleFault_Handler(__attribute__((unused)) interrupt_frame* frame) {
	Panic("Double Fault Detected");
	asm("hlt");
}

__attribute__((interrupt)) void GPFault_Handler(__attribute__((unused)) interrupt_frame* frame) {
	Panic("General Protection Fault Detected");
	asm("hlt");
}

__attribute__((interrupt)) void KeyboardInt_Handler(__attribute__((unused)) interrupt_frame* frame) {
	uint8_t scanCode = inb(0x60);

	HandleKeyboard(scanCode);

	PIC_EndMaster();
}

__attribute__((no_caller_saved_registers)) void PIC_EndMaster() {
	outb(PIC1_COMMAND, PIC_EOI);
}

__attribute__((unused)) void PIC_EndSlave() {
	outb(PIC2_COMMAND, PIC_EOI);
	outb(PIC1_COMMAND, PIC_EOI);
}

void RemapPIC() {
	uint8_t a1, a2;

	a1 = inb(PIC1_DATA);
	io_wait();
	a2 = inb(PIC2_DATA);
	io_wait();

	outb(PIC1_COMMAND, ICW1_INIT | ICW1_ICW4);
	io_wait();
	outb(PIC2_COMMAND, ICW1_INIT | ICW1_ICW4);
	io_wait();

	outb(PIC1_DATA, 0x20);
	io_wait();
	outb(PIC2_DATA, 0x28);
	io_wait();

	outb(PIC1_DATA, 4);
	io_wait();
	outb(PIC2_DATA, 2);
	io_wait();

	outb(PIC1_DATA, ICW4_8086);
	io_wait();
	outb(PIC2_DATA, ICW4_8086);
	io_wait();

	outb(PIC1_DATA, a1);
	io_wait();
	outb(PIC2_DATA, a2);
	io_wait();
}
#pragma clang diagnostic pop