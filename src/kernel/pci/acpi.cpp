#include "acpi.hpp"

namespace ACPI {
	void* FindTable(SDTHeader* sdtHeader, const char* signature) {
		int entries = static_cast<int>(sdtHeader->Length - sizeof(ACPI::SDTHeader)) / 8;

		for (int t = 0; t < entries; ++t) {
			auto* newSDTHeader = reinterpret_cast<ACPI::SDTHeader*>(*reinterpret_cast<uint64_t*>(reinterpret_cast<uint64_t>(sdtHeader) + sizeof(ACPI::SDTHeader) + (t * 8)));
			for (int i = 0; i < 4; ++i) {
				if (newSDTHeader->Signature[i] != signature[i]) {
					break;
				}
				if (i == 3) return newSDTHeader;
			}
		}
		return nullptr;
	}
}