#pragma once
#include <cstdint>
#include "acpi.hpp"

namespace PCI {
	struct PCIDeviceHeader {
		uint16_t VendorID;
		uint16_t DeviceID;
		uint16_t Command;
		uint16_t Status;
		uint8_t RevisionID;
		uint8_t ProgramInterface;
		uint8_t Subclass;
		uint8_t Class;
		uint8_t CacheLineSize;
		uint8_t LatencyTimer;
		uint8_t HeaderType;
		uint8_t BIST;

		uint32_t BAR[6];

		uint32_t CardbusCISPointer;
		uint16_t SubsystemVendorID;
		uint16_t SubsystemID;
		uint32_t ExpansionROMBaseAddress;
		uint8_t CapabilitiesPtr;
		uint8_t Reserved[3+4];
		uint8_t InterruptLine;
		uint8_t InterruptPIN;
		uint8_t MinGrant;
		uint8_t MaxLatency;
	} __attribute__((packed));

	void EnumeratePCI(ACPI::MCFGHeader* mcfg);

	extern const char* DeviceClasses[];

	const char* GetVendorName(uint16_t vendorID);
	const char* GetDeviceName(uint16_t vendorID, uint16_t deviceID);
	const char* GetSubclassName(uint8_t classCode, uint8_t subclassCode);
	const char* GetProgIFName(uint8_t classCode, uint8_t subclassCode, uint8_t progIF);
}