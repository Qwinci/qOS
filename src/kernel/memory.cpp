#include "include/memory.hpp"

uint64_t GetMemorySize(EFI_MEMORY_DESCRIPTOR* mMap, uint64_t mMapSize, uint64_t mMapDescSize) {
	static uint64_t memorySizeBytes = 0;
	if (memorySizeBytes > 0) {
		return memorySizeBytes;
	}

	auto* offset = reinterpret_cast<uint8_t*>(mMap);
	auto* endOfMap = reinterpret_cast<uint8_t*>(mMap) + mMapSize;

	while (offset < endOfMap) {
		auto* desc = reinterpret_cast<EFI_MEMORY_DESCRIPTOR*>(offset);
		memorySizeBytes += desc->NumberOfPages * 4096;

		offset += mMapDescSize;
	}

	return memorySizeBytes;
}