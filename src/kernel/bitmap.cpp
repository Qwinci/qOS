#include "include/bitmap.hpp"

bool Bitmap::operator[](uint64_t index) const {
	if (index > Size * 8) return false;
	uint64_t byteIndex = index / 8;
	uint8_t bitIndex = index % 8;
	uint8_t bitIndexer = 0b10000000 >> bitIndex;
	if ((Buffer[byteIndex] & bitIndexer) > 0) {
		return true;
	}
	else {
		return false;
	}
}

bool Bitmap::Set(uint64_t index, bool value) const {
	if (index > Size * 8) return false;
	uint64_t byteIndex = index / 8;
	uint8_t bitIndex = index % 8;
	uint8_t bitIndexer = 0b10000000 >> bitIndex;
	Buffer[byteIndex] &= ~bitIndexer;
	if (value) {
		Buffer[byteIndex] |= bitIndexer;
	}
	return true;
}