#include <cstddef>

inline int memcmp(const void*aptr, const void* bptr, size_t n) {return __builtin_memcmp(aptr, bptr, n);}
inline void* memcpy(void* destination, const void* source, size_t num) {return __builtin_memcpy(destination, source, num);}
inline void* memset(void* ptr, int value, size_t num) {return __builtin_memset(ptr, value, num);}