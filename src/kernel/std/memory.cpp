#include <cstddef>

__attribute__((no_builtin("memcpy"))) void* memcpy(void* destination, const void* source, size_t num) {
	for (size_t i = 0; i < num; ++i) {
		static_cast<char*>(destination)[i] = static_cast<const char*>(source)[i];
	}
	return destination;
}

extern "C" __attribute__((no_builtin("memset"))) void* memset(void* ptr, int value, size_t num) {
	for (size_t i = 0; i < num; ++i) {
		static_cast<char*>(ptr)[i] = static_cast<unsigned char>(value); // NOLINT(cppcoreguidelines-narrowing-conversions)
	}
	return ptr;
}
