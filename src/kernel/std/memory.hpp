#pragma once
#include <cstdint>

inline int memcmp(const void* aptr, const void* bptr, size_t n) {return __builtin_memcmp(aptr, bptr, n);}
inline void* memcpy(void* destination, const void* source, size_t num) {return __builtin_memcpy(destination, source, num);}
inline void memset(void* start, uint8_t value, uint64_t num) {
#pragma clang loop unroll(full)
	for (uint64_t i = 0; i < num / 8; ++i) {
		*(uint64_t*)((uint64_t)start + i) = value;
	}
}