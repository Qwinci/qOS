#include "include/frameBuffer.hpp"
#include "include/simpleFont.hpp"
#include "include/basicRenderer.hpp"
#include <cstddef>

BasicRenderer* GlobalRenderer;

void BasicRenderer::PutChar(char chr, uint32_t xOff, uint32_t yOff) const {
	auto* pixelPtr = static_cast<uint32_t *>(targetFrameBuffer->BaseAddress);
	char* fontPtr = static_cast<char*>(font->glyphBuffer) + (chr * font->psf1Header->charSize);
	for (unsigned long y = yOff; y < yOff + 16; ++y) {
		for (unsigned long x = xOff; x < xOff + 8; ++x) {
			if ((*fontPtr & (0b10000000 >> (x - xOff))) > 0) {
				*(unsigned int*)(pixelPtr + x + (y * targetFrameBuffer->PixelsPerScanline)) = color;
			}
		}
		++fontPtr;
	}
}
void BasicRenderer::Print(const char* str) {
	char* chr = const_cast<char*>(str);
	while (*chr != 0) {
		PutChar(*chr, cursorPosition.x, cursorPosition.y);
		cursorPosition.x += 8;
		if (cursorPosition.x + 8 > targetFrameBuffer->Width) {
			cursorPosition.x = 0;
			cursorPosition.y += 16;
		}
		++chr;
	}
}

void BasicRenderer::Clear() const {
	auto fbBase = (uint64_t) targetFrameBuffer->BaseAddress;
	uint64_t bytesPerScanline = targetFrameBuffer->PixelsPerScanline * 4;
	uint64_t fbHeight = targetFrameBuffer->Height;
	uint64_t fbSize = targetFrameBuffer->BufferSize;

	for (int verticalScanline = 0; verticalScanline < fbHeight; ++verticalScanline) {
		uint64_t pixPtrBase = fbBase + (bytesPerScanline * verticalScanline);
		for (auto* pixPtr = (uint32_t*) pixPtrBase; pixPtr < (uint32_t*)(pixPtrBase + bytesPerScanline); ++pixPtr) {
			*pixPtr = clearColor;
		}
	}
}

static size_t putCharCount = 0;
void BasicRenderer::ClearChar() {
	if (putCharCount != 0) {
		if (cursorPosition.x == 0) {
			cursorPosition.x = (static_cast<long>(putCharCount) * 8 % targetFrameBuffer->Width) == 0
			                   ? targetFrameBuffer->Width : static_cast<long>(putCharCount) * 8 % targetFrameBuffer->Width;
			cursorPosition.y -= 16;
			if (cursorPosition.y < 0) cursorPosition.y = 0;
		}

		unsigned int xOff = cursorPosition.x;
		unsigned int yOff = cursorPosition.y;

		auto* pixelPtr = static_cast<uint32_t*>(targetFrameBuffer->BaseAddress);
		for (unsigned long y = yOff; y < yOff + 16; ++y) {
			for (unsigned long x = xOff - 8; x < xOff; ++x) {
				*(unsigned int*)(pixelPtr + x + (y * targetFrameBuffer->PixelsPerScanline)) = clearColor;
			}
		}

		cursorPosition.x -= 8;

		if (cursorPosition.x < 0) {
			cursorPosition.x = (static_cast<long>(putCharCount - 1) * 8 % targetFrameBuffer->Width) == 0
					? targetFrameBuffer->Width : static_cast<long>(putCharCount - 1) * 8 % targetFrameBuffer->Width;
			cursorPosition.y -= 16;
			if (cursorPosition.y < 0) cursorPosition.y = 0;
		}

		--putCharCount;
	}
}

void BasicRenderer::Next() {
	cursorPosition = {0, cursorPosition.y + 16};
}

void BasicRenderer::PutChar(char chr) {
	++putCharCount;
	PutChar(chr, cursorPosition.x, cursorPosition.y);
	cursorPosition.x += 8;
	if (cursorPosition.x + 8 > targetFrameBuffer->Width) {
		cursorPosition = {0, cursorPosition.y + 16};
	}
}
