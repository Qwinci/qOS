#include "heap.hpp"
#include "../paging/pageTableManager.hpp"
#include "../paging/pageframeallocator.hpp"

static void* heapStart;
static void* heapEnd;
static HeapSegHdr* lastHdr;

void* operator new(size_t size) {return malloc(size);}
void* operator new[](size_t size) {return malloc(size);}
void operator delete(void* p) { free(p);}
void operator delete[](void* p) { free(p);}

void InitializeHeap(void* heapAddress, size_t pageCount) {
	void* pos = heapAddress;

	for (size_t i = 0; i < pageCount; ++i) {
		g_PageTableManager.MapMemory(pos, GlobalAllocator.RequestPage());
		pos = reinterpret_cast<void*>(reinterpret_cast<size_t>(pos) + 0x1000);
	}

	size_t heapLength = pageCount * 0x1000;

	heapStart = heapAddress;
	heapEnd = reinterpret_cast<void*>(reinterpret_cast<size_t>(heapStart) + heapLength);
	auto* startSeg = static_cast<HeapSegHdr*>(heapAddress);
	startSeg->length = heapLength - sizeof(HeapSegHdr);
	startSeg->next = nullptr;
	startSeg->last = nullptr;
	startSeg->free = true;
	lastHdr = startSeg;
}

void* malloc(size_t size) {
	if ((size & 0b1111) > 0) {
		size -= size & 0b1111;
		size += 0x10;
	}

	if (size == 0) return nullptr;

	auto* currentSeg = static_cast<HeapSegHdr*>(heapStart);
	while (true) {
		if (currentSeg->free) {
			if (currentSeg->length > size) {
				currentSeg->Split(size);
				currentSeg->free = false;
				return currentSeg + 1;
			}
			if (currentSeg->length == size) {
				currentSeg->free = false;
				return currentSeg + 1;
			}
		}
		if (currentSeg->next == nullptr) break;
		currentSeg = currentSeg->next;
	}
	ExpandHeap(size);
	return malloc(size);
}

void free(void* address) {
	HeapSegHdr* segment = (HeapSegHdr*)address - 1;
	segment->free = true;
	segment->CombineForward();
	segment->CombineBackward();
}

HeapSegHdr *HeapSegHdr::Split(size_t splitLength) {
	if (splitLength < 0x10) return nullptr;
	int64_t splitSegLength = length - splitLength - (sizeof(HeapSegHdr));
	if (splitSegLength < 0x10) return nullptr;

	auto* newSplitHdr = (HeapSegHdr*) ((size_t)this + splitLength + sizeof(HeapSegHdr));
	next->last = newSplitHdr;
	newSplitHdr->next = next;
	next = newSplitHdr;
	newSplitHdr->last = this;
	newSplitHdr->length = splitSegLength;
	newSplitHdr->free = free;
	length = splitLength;

	if (lastHdr == this) lastHdr = newSplitHdr;
	return newSplitHdr;
}

void ExpandHeap(size_t length) {
	if (length & 0xFFF) {
		length -= length & 0xFFF;
		length += 0x1000;
	}

	size_t pageCount = length / 0x1000;
	auto* newSegment = static_cast<HeapSegHdr*>(heapEnd);

	for (size_t i = 0; i < pageCount; ++i) {
		g_PageTableManager.MapMemory(heapEnd, GlobalAllocator.RequestPage());
		heapEnd = reinterpret_cast<void*>(reinterpret_cast<size_t>(heapEnd) + 0x1000);
	}

	newSegment->free = true;
	newSegment->last = lastHdr;
	lastHdr->next = newSegment;
	lastHdr = newSegment;
	newSegment->next = nullptr;
	newSegment->length = length - sizeof(HeapSegHdr);
	newSegment->CombineBackward();
}

void HeapSegHdr::CombineForward() {
	if (next == nullptr) return;
	if (!next->free) return;

	if (next == lastHdr) lastHdr = this;

	if (next->next != nullptr) {
		next->next->last = this;
	}

	next = next->next;

	length = length + next->length + sizeof(HeapSegHdr);
}

void HeapSegHdr::CombineBackward() const {
	if (last != nullptr && last->free) last->CombineForward();
}
