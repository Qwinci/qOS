#pragma once
#include <cstddef>

struct HeapSegHdr {
	size_t length;
	HeapSegHdr* next;
	HeapSegHdr* last;
	bool free;
	void CombineForward();
	void CombineBackward() const;
	HeapSegHdr* Split(size_t splitLength);
};

void InitializeHeap(void* heapAddress, size_t pageCount);

void* malloc(size_t size);
void free(void* address);

void ExpandHeap(size_t length);

/*extern void* operator new(size_t size);
extern void* operator new[](size_t size);
extern void operator delete(void* p);
extern void operator delete[](void* p);*/