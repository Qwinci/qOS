#pragma once
#include "math.hpp"
#include "cstdint"

struct FrameBuffer;
struct PSF1_FONT;

class BasicRenderer {
public:
	Point cursorPosition;
	FrameBuffer* targetFrameBuffer;
	PSF1_FONT* font;
	uint32_t color{0xFFFFFFFF};
	uint32_t clearColor{0x00000000};

	BasicRenderer() : cursorPosition{0, 0}, targetFrameBuffer{nullptr}, font{nullptr} {}
	BasicRenderer(FrameBuffer* targetFrameBuffer, PSF1_FONT* font, uint32_t x, uint32_t y) : cursorPosition{x, y}, targetFrameBuffer{targetFrameBuffer}, font{font} {}

	void PutChar(char chr, uint32_t xOff, uint32_t yOff) const;
	void PutChar(char chr);
	void Print(const char* str);
	void ClearChar();
	void Clear() const;
	void Next();
};

extern BasicRenderer* GlobalRenderer;