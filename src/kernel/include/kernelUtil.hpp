#pragma once

#include "frameBuffer.hpp"
#include "simpleFont.hpp"
#include "basicRenderer.hpp"
#include "efiMemory.hpp"
#include "../paging/pageframeallocator.hpp"
#include "../paging/paging.hpp"
#include "../std/memory.hpp"
#include "../paging/pageTableManager.hpp"
#include "../pci/acpi.hpp"

struct BootInfo {
	FrameBuffer* frameBuffer;
	PSF1_FONT* font;
	EFI_MEMORY_DESCRIPTOR* mMap;
	uint64_t mapSize;
	uint64_t mapDescSize;
	ACPI::RSDP2* rsdp;
};

extern uint64_t _KernelStart; // NOLINT(bugprone-reserved-identifier)
extern uint64_t _KernelEnd; // NOLINT(bugprone-reserved-identifier)

struct KernelInfo {
	PageTableManager* pageTableManager;
};

KernelInfo InitializeKernel(BootInfo* bootInfo);