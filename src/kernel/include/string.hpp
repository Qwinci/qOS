#pragma once
#include "cstdint"

const char* to_string(uint64_t integer);
const char* to_string(int64_t integer);

const char* to_hstring(uint64_t integer);

const char* to_string(double value, uint8_t decimalPlaces = 2);