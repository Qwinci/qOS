#pragma once
#include "cstdint"

#pragma pack(push, 1)
struct PSF1_HEADER {
	uint8_t magic[2];
	uint8_t mode;
	uint8_t charSize;
};
#pragma pack(pop)
struct PSF1_FONT {
	PSF1_HEADER* psf1Header;
	void* glyphBuffer;
};