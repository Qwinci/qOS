#pragma once

struct FrameBuffer {
	void* BaseAddress;
	unsigned long long BufferSize;
	unsigned int Width;
	unsigned int Height;
	unsigned int PixelsPerScanline;
};