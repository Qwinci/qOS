#pragma once

__attribute__((no_caller_saved_registers)) void Panic(const char* panicMessage);