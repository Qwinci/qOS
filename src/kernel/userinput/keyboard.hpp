#pragma once
#include <cstdint>
#include "kbScancodeTranslation.hpp"
#include "../include/basicRenderer.hpp"

void HandleKeyboard(uint8_t scancode);