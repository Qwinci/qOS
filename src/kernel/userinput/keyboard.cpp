#include "keyboard.hpp"

static bool isLeftShiftPressed;
static bool isRightShiftPressed;

void HandleKeyboard(uint8_t scancode) {

	switch (scancode) {
		case LeftShift: // pressed
			isLeftShiftPressed = true;
			return;
		case LeftShift + 0x80: // released
			isLeftShiftPressed = false;
			return;
		case RightShift:
			isRightShiftPressed = true;
			return;
		case RightShift + 0x80:
			isRightShiftPressed = false;
			return;
		case Enter:
			GlobalRenderer->Next();
			return;
		case Spacebar:
			GlobalRenderer->PutChar(' ');
			return;
		case BackSpace:
			GlobalRenderer->ClearChar();
			return;
	}

	char ascii = QWERTYKeyboard::Translate(scancode, isLeftShiftPressed | isRightShiftPressed);

	if (ascii != 0) {
		GlobalRenderer->PutChar(ascii);
	}
}