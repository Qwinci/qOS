#include "include/kernelUtil.hpp"

extern "C" {
void start(BootInfo* bootInfo) {
	KernelInfo kernelInfo = InitializeKernel(bootInfo);
	PageTableManager* pageTableManager = kernelInfo.pageTableManager;

	GlobalRenderer->Print("Kernel initialized successfully");
	GlobalRenderer->Next();

	while (true) {
		asm("hlt");
	}
}
}
