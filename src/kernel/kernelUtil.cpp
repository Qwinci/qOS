#include "include/kernelUtil.hpp"
#include "gdt/gdt.hpp"
#include "interrupts/IDT.hpp"
#include "interrupts/interrupts.hpp"
#include "include/IO.hpp"
#include "include/string.hpp"
#include "pci/pci.hpp"
#include "memory/heap.hpp"

KernelInfo kernelInfo;

BasicRenderer r = BasicRenderer(nullptr, nullptr, 0, 0); // NOLINT(cert-err58-cpp)
void PrepareMemory(BootInfo* bootInfo) {
	r = BasicRenderer(bootInfo->frameBuffer, bootInfo->font, 0, 0);
	GlobalRenderer = &r;

	GlobalAllocator = PageFrameAllocator();
	GlobalAllocator.ReadEFIMemoryMap(bootInfo->mMap, bootInfo->mapSize, bootInfo->mapDescSize);

	uint64_t kernelSize = &_KernelEnd - &_KernelStart;
	uint64_t kernelPages = kernelSize / 4096 + 1;
	GlobalAllocator.LockPages(&_KernelStart, kernelPages);
	auto* PML4 = static_cast<PageTable*>(GlobalAllocator.RequestPage());
	memset(PML4, 0, 0x1000);
	g_PageTableManager = PageTableManager(PML4);

	for (uint64_t i = 0; i < GetMemorySize(bootInfo->mMap, bootInfo->mapSize, bootInfo->mapDescSize); i += 0x1000) {
		g_PageTableManager.MapMemory(reinterpret_cast<void*>(i), reinterpret_cast<void*>(i));
	}

	auto fbBase = reinterpret_cast<uint64_t>(bootInfo->frameBuffer->BaseAddress);
	uint64_t fbSize = bootInfo->frameBuffer->BufferSize + 0x1000;
	GlobalAllocator.LockPages(reinterpret_cast<void*>(fbBase), fbSize / 0x1000 + 1);
	for (uint64_t i = fbBase; i < fbBase + fbSize; i += 4096) {
		g_PageTableManager.MapMemory(reinterpret_cast<void*>(i), reinterpret_cast<void*>(i));
	}
	asm("mov %0, %%cr3" : : "r"(PML4));

	kernelInfo.pageTableManager = &g_PageTableManager;
}

IDTR idtr;
void SetIDTGate(void(*handler)(interrupt_frame *frame), uint8_t entryOffset, uint8_t type_attr, uint8_t selector) {
	auto* interrupt = (IDTDescEntry*)(idtr.Offset + entryOffset * sizeof(IDTDescEntry));
	interrupt->SetOffset((uint64_t) handler);
	interrupt->type_attr = type_attr;
	interrupt->selector = selector;
}

void PrepareInterrupts() {
	idtr.Limit = 0x0FFF;
	idtr.Offset = (uint64_t) GlobalAllocator.RequestPage();

	SetIDTGate(PageFault_Handler, 0xE, IDT_TA_InterruptGate, 0x08);
	SetIDTGate(DoubleFault_Handler, 0x8, IDT_TA_InterruptGate, 0x08);
	SetIDTGate(GPFault_Handler, 0xD, IDT_TA_InterruptGate, 0x08);
	SetIDTGate(KeyboardInt_Handler, 0x21, IDT_TA_InterruptGate, 0x08);

	asm("lidt %0" : : "m"(idtr));

	RemapPIC();
}

void PrepareACPI(BootInfo* bootInfo) {
	auto* xsdt = reinterpret_cast<ACPI::SDTHeader*>(bootInfo->rsdp->XSDTAddress);

	auto* mcfg = static_cast<ACPI::MCFGHeader*>(ACPI::FindTable(xsdt, "MCFG"));
	return;

	PCI::EnumeratePCI(mcfg);
}

KernelInfo InitializeKernel(BootInfo* bootInfo) {
	GDTDescriptor gdtDescriptor{};
	gdtDescriptor.Size = sizeof(GDT) - 1;
	gdtDescriptor.Offset = (uint64_t)&DefaultGDT;
	LoadGDT(&gdtDescriptor);

	PrepareMemory(bootInfo);

	memset(bootInfo->frameBuffer->BaseAddress, 0, bootInfo->frameBuffer->BufferSize);

	InitializeHeap(reinterpret_cast<void*>(0x0000100000000000), 0x20);

	PrepareInterrupts();

	PrepareACPI(bootInfo);

	outb(PIC1_DATA, 0b11111001);
	outb(PIC2_DATA, 0b11101111);

	asm("sti");

	return kernelInfo;
}