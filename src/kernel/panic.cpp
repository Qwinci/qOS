#include "include/panic.hpp"
#include "include/basicRenderer.hpp"
#include "include/string.hpp"

__attribute__((no_caller_saved_registers)) void Panic(const char* panicMessage) {
	GlobalRenderer->clearColor = 0x00FF0000;
	GlobalRenderer->Clear();
	GlobalRenderer->cursorPosition = {0, 0};

	GlobalRenderer->color = 0;

	GlobalRenderer->Print("Kernel Panic");

	GlobalRenderer->Next();
	GlobalRenderer->Next();

	GlobalRenderer->Print(panicMessage);
	GlobalRenderer->Next();
}